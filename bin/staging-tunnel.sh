#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

GITLAB_USER=${GITLAB_USER:-$USER}
ETC_HOSTS=/etc/hosts
IP="127.0.0.1"
GITLAB_HOSTNAME=${GITLAB_HOSTNAME:-staging.gitlab.com}
BASTION_HOST=${BASTION_HOST:-lb-bastion.gstg.gitlab.com}
TUNNEL_TARGET_HOST=${TUNNEL_TARGET_HOST:-fe-01-lb-gstg.c.gitlab-staging-1.internal}

function remove_host() {
    if [[ -n "$(grep $GITLAB_HOSTNAME /etc/hosts)" ]]
    then
        echo "$GITLAB_HOSTNAME Found in your $ETC_HOSTS, Removing now...";
        sudo sed -i".bak" "/$GITLAB_HOSTNAME/d" $ETC_HOSTS
    else
        echo "$GITLAB_HOSTNAME was not found in your $ETC_HOSTS";
    fi
}

function add_host() {
    HOSTS_LINE="$IP\t$GITLAB_HOSTNAME"
    if [[ -n "$(grep $GITLAB_HOSTNAME /etc/hosts)" ]]
    then
      echo "$GITLAB_HOSTNAME already exists : $(grep $GITLAB_HOSTNAME $ETC_HOSTS)"
    else
      echo "Adding $GITLAB_HOSTNAME to your $ETC_HOSTS";
      sudo -- sh -c -e "echo '$HOSTS_LINE' >> /etc/hosts";

      if [[ -n "$(grep $GITLAB_HOSTNAME /etc/hosts)" ]]
      then
        echo "$GITLAB_HOSTNAME was added successfully";
        grep $GITLAB_HOSTNAME /etc/hosts;
      else
        echo "Failed to Add $GITLAB_HOSTNAME, Try again!";
      fi
    fi
}


# Make sure ssh works before
ssh ${GITLAB_USER}@${BASTION_HOST} date

sudo echo sudo obtained

echo "Logging into staging bastion as $GITLAB_USER"

ssh -N -L 8443:${TUNNEL_TARGET_HOST}:443 -L 8822:${TUNNEL_TARGET_HOST}:22 ${GITLAB_USER}@${BASTION_HOST} &

echo Running socat listeners

sudo socat -d TCP4-LISTEN:443,fork TCP4-CONNECT:localhost:8443 &
SOCAT_PID_HTTPS=$!
echo "socat HTTPS PID $SOCAT_PID_HTTPS"

sudo socat -d TCP4-LISTEN:22,fork TCP4-CONNECT:localhost:8822 &
SOCAT_PID_SSH=$!
echo "socat SSH PID $SOCAT_PID_SSH"

function finish {
  remove_host
  echo killing ssh tunnels
  kill %1
  echo killing socat HTTPS
  sudo kill $SOCAT_PID_HTTPS
  echo killing socat SSH
  sudo kill $SOCAT_PID_SSH
}
trap finish EXIT


add_host

echo "Now visit https://${GITLAB_HOSTNAME}"
echo "Press any key to close the tunnel"
read
