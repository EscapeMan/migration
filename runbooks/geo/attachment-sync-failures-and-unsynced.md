# Resolve attachment failures and stuck unsynced

## First and foremost

*Don't Panic*

## Diagnose errors

- Visit [Kibana](https://log.gitlab.net/app/kibana)
- Select `pubsub-geo-inf-gprd`
- Search for `"Unsuccessful download"` to get non-success responses from the primary
- Search for `"Error downloading file"` to get exceptions during transfer
- Search for `"Error creating temporary file"`
- Search for `"Destination file is a directory"`
- Search for `"File download"` to get `FileDownloadService` results

## Get failed attachment registries

```ruby
failed_registries = Geo::FileRegistry.attachments.failed; nil
```

## Get unsynced uploads

```ruby
unsynced_uploads = Geo::AttachmentRegistryFinder.new.find_unsynced(batch_size: 10); nil
```

## Resync registries

```ruby
failed_registries.each do |f|
  Geo::FileDownloadService.new(f.file_type.to_sym, f.file_id).execute

  puts "#{f.reload.success ? 'Success' : 'Failed'}: #{f.inspect}"
end
```

## Sync unsynced uploads

```ruby
unsynced_uploads.each do |upload|
  file_type = upload.uploader.sub(/Uploader\z/, '').underscore.to_sym

  Geo::FileDownloadService.new(file_type, upload.id).execute

  registry = Geo::FileRegistry.find_by(file_type: file_type, file_id: upload.id)
  puts "#{registry.success ? 'Success' : 'Failed'}: #{registry.inspect}"
end
```

## Transfer an upload even more manually

```ruby
file_type = upload.uploader.sub(/Uploader\z/, '').underscore.to_sym
transfer = Gitlab::Geo::FileTransfer.new(file_type, upload)
transfer.download_from_primary
```
